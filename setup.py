from setuptools import setup

setup(
    name='pgzgrid',
    version='0.0.5',
    description='Tile-oriented micro-engine for PyGame Zero',
    url='http://bitbucket.org/dogwynn/pgzgrid',
    author='David O\'Gwynn',
    author_email='dogwynn@fastmail.com',
    license='MIT',
    packages=['pgzgrid'],
    install_requires=[
        'pillow',
        'memoized_property',
        'astar',
        'requests',
        'shapely',
    ],
    zip_safe=False,
)
