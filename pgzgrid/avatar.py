import math
import logging
import inspect

from shapely import geometry
import pygame
from pygame.math import Vector2
from pgzero.actor import Actor

from .helpers import (
    clamp, init_image, diffprint, pgzglobals,
)
from . import view

log = logging.getLogger('grid')
log.addHandler(logging.NullHandler())

class Boundary(dict):
    def __init__(self, *kw):
        self.update({
            'top': False,
            'right': False,
            'bottom': False,
            'left': False,
        })
        self.update(kw)
    @property
    def top(self):
        return self['top']
    @property
    def right(self):
        return self['right']
    @property
    def bottom(self):
        return self['bottom']
    @property
    def left(self):
        return self['left']

class BarrierCollision(dict):
    def __init__(self, *kw):
        self.update({
            'top': set(),
            'right': False,
            'bottom': False,
            'left': False,
        })
        self.update(kw)

class AvatarImageError(Exception):
    pass
        
class Avatar(Actor):
    is_player = False
    is_hostile = False
    is_mob = False
    is_item = False
    alive = True
    deleted = False
    start_image = None
    start_view = None
    clock = None
    clipping = True
    gravity = Vector2(0, 0)
    max_vector = Vector2(0.1, 0.2)
    scale = 1.0
    rotate = 0
    flip = False
    def __init__(self, control, cr, *,
                 image=None, gravity=None, max_vector=None,
                 scale=None, rotate=None, **kw):
        self.control = control
        self.cr = Vector2(cr)
        self.start_point = Vector2(self.cr)

        if gravity is None:
            gravity = self.__class__.gravity
        self.gravity = Vector2(gravity)

        if max_vector is None:
            max_vector = self.__class__.max_vector
        self.max_vector = Vector2(max_vector)

        self.vector = Vector2()
        pos = self.control.view.get_xy(*cr)

        if scale is not None:
            self.scale = scale

        if rotate is not None:
            self.rotate = rotate

        if image is None:
            image = self.start_image
        if image is None:
            raise AvatarImageError(
                'Must provide image at class level (start_image)'
                ' or in constructor with image keyword'
            )
        image = init_image(image, self.scale)

        self.projectiles = []
        
        super().__init__(image, pos, **kw)

    @property
    def int_cr(self):
        return Vector2(*map(int, self.cr))

    _vector = None
    _need_adjust_c = False
    _need_adjust_r = False
    _queued_moves = None
    vector_projection = 'circle'
    @property
    def vector(self):
        if self.vector_projection == 'circle':
            a, b = self.max_vector
            c0, r0 = self._vector
            if (c0**2/a**2 + r0**2/b**2) >= 1:
                theta = math.atan2(r0, c0)
                k = 1 / (b**2 * math.cos(theta)**2 +
                         a**2 * math.sin(theta)**2)**0.5
                c = k * a * b * math.cos(theta)
                r = k * a * b * math.sin(theta)
                self._vector = Vector2(c, r)
        elif self.vector_projection == 'rect':
            mc, mr = self.max_vector
            box = geometry.box(
                -mc, -mr, mc, mr
            )
            l = geometry.LineString([(0, 0), self._vector])
            if box.boundary.intersects(l):
                intersect = list(box.boundary.intersection(l).coords)
                self._vector = Vector2(intersect[0])
        return self._vector
    @vector.setter
    def vector(self, vector):
        vector = Vector2(vector)
        c, r = vector
        mc, mr = self.max_vector
        self._vector = Vector2(c, r)
        if abs(self._vector[0]) < 0.01:
            self._need_adjust_c = True
        if abs(self._vector[1]) < 0.01:
            self._need_adjust_r = True

    _impulse = False
    def move(self, vector):
        self._impulse = True
        if self._queued_moves is None:
            self._queued_moves = []
        v = Vector2(vector)
        self._vector = self._vector + v
        if self.view_vector + v == Vector2(0, 0):
            self.view_vector = v
        else:
            self.view_vector += v

    _view_vector = None
    @property
    def view_vector(self):
        if self._view_vector is None:
            self._view_vector = Vector2(1, 0)
        return Vector2(self._view_vector)

    @view_vector.setter
    def view_vector(self, vector):
        v = Vector2(vector)
        self._view_vector = v.normalize()

    @property
    def view_angle(self):
        view = self.view_vector
        view[-1] = -view[-1]
        angle = view.as_polar()[-1]
        return angle

    @property
    def angle_vector(self):
        view = Vector2(0, 0)
        view.from_polar((1, self.angle + self.rotate))
        view[1] = -view[1]
        return view

    def view_distance_to(self, other):
        col, row = self.cr
        ocol, orow = other.cr
        return math.hypot(col - ocol, row - orow)

    def fire_projectile(self, projectile_class):
        col, row = self.cr
        angle = self.view_angle
        projectile = projectile_class(
            self, self.control, (col, row),
        )
        projectile.angle = angle
        mv = Vector2(projectile.max_vector)
        projectile.move(self.view_vector * mv.length())
        self.projectiles.append(projectile)

    halo_dist=0.1
    def halo(self, cr, halo_dist=None):
        halo_dist = self.halo_dist if halo_dist is None else halo_dist
        x, y = self.control.view.get_xy(*cr)
        x -= self.width/2
        y -= self.height/2
        rect = pygame.Rect((x,y), (self.width, self.height))
        tw, th = self.control.grid.get_tile_size()

        return {
            'top': [
                # (rect.left, rect.top - th * halo_dist),
                (rect.centerx, rect.top - th * halo_dist),
                # (rect.right, rect.top - th * halo_dist),
            ],
            'bottom': [
                # (rect.left, rect.bottom + th * halo_dist),
                (rect.centerx, rect.bottom + th * halo_dist),
                # (rect.right, rect.bottom + th * halo_dist),
            ],
            'left': [
                # (rect.left - tw * halo_dist, rect.top),
                (rect.left - tw * halo_dist, rect.centery),
                # (rect.left - tw * halo_dist, rect.bottom),
            ],
            'right': [
                # (rect.right + tw * halo_dist, rect.top),
                (rect.right + tw * halo_dist, rect.centery),
                # (rect.right + tw * halo_dist, rect.bottom),
            ],
        }

    def check_barriers(self, cr=None):
        cr = self.cr if cr is None else cr
        sides = {}
        for side, points in self.halo(cr).items():
            for p in points:
                for (sq, rect) in self.control.view.barriers:
                    if rect.collidepoint(*p):
                        sides.setdefault(side, set()).add(sq)
        return {k: list(squares) for k,squares in sides.items()}

    def check_mobs(self):
        collide = [m for m in self.control.mobs if m.colliderect(self)]
        return collide

    def check_player(self):
        if not self.is_player:
            return self.colliderect(self.control.player)

    def check_items(self):
        return [i for i in self.control.items if i.colliderect(self)]

    def check_transitions(self):
        for tr, sq in self.control.grid.transitions:
            if self.int_cr == sq.int_cr:
                return (tr, sq)

    def check_boundaries(self, cr=None):
        cr = self.cr if cr is None else cr
        sides = {}
        for side, points in self.halo(cr).items():
            crs = [self.control.view.get_cr(*p) for p in points]
            for (c, r) in crs:
                if c < 0:
                    sides.setdefault(side, set()).add('left')
                if c >= self.control.view.grid.cols:
                    sides.setdefault(side, set()).add('right')
                if r < 0:
                    sides.setdefault(side, set()).add('top')
                if r >= self.control.view.grid.rows:
                    sides.setdefault(side, set()).add('bottom')
        return sides

    def draw(self):
        for p in self.projectiles:
            p.draw()
        super().draw()
        if view.debug:
            for side, points in self.halo(self.cr).items():
                for p in points:
                    pgzglobals.screen.draw.filled_circle(p, 3, 'red')
            pgzglobals.screen.draw.filled_circle(self.pos, 5, 'red')
            dc, dr = 40 * self._vector
            c, r = self.cr
            x, y = self.control.view.get_xy(c+dc, r+dr)
            pgzglobals.screen.draw.line(self.pos, (x, y), 'red')

    _rotate_angle_set = False
    _start_view_set = False
    _entered_transition = False
    _started_transition = False
    def update(self):
        if self.rotate is not None and not self._rotate_angle_set:
            self.angle = self.rotate
            self._rotate_angle_set = True
        if self.start_view is not None and not self._start_view_set:
            self.view_vector = self.start_view
            self._start_view_set = True
        
        transition = self.check_transitions()
        cls = self.__class__
        if self.is_player:
            if transition:
                if cls._entered_transition:
                    if (self.vector == (0, 0) and
                        not cls._started_transition):
                        cls._started_transition = True
                        (tr, sq) = transition
                        self.control.transition_level(tr, sq, self)
                else:
                    cls._entered_transition = True
            else:
                if cls._started_transition:
                    cls._started_transition = False
                cls._entered_transition = False
            diffprint((cls._entered_transition, cls._started_transition))

        self._update_track()
        
        barriers = self.check_barriers()
        boundaries = self.check_boundaries()
        sides = set(barriers) | set(boundaries)

        vector = Vector2(*self.vector)

        do_gravity = 'bottom' not in barriers and self.gravity
        if do_gravity:
            vector += self.gravity

        dc, dr = vector
        if self.clipping:
            if 'bottom' in sides:
                if ('bottom' in barriers or
                    'bottom' in boundaries.get('bottom',set())):
                    vector[1] = min(0, dr)

            if 'top' in sides:
                if ('top' in barriers or
                    'top' in boundaries.get('top',set())):
                    if dr < 0:
                        vector[1] = 0

            if 'left' in sides:
                if dc < 0:
                    vector[0] = 0

            if 'right' in sides:
                if dc > 0:
                    vector[0] = 0

            if 'bottom' in barriers:
                sq = barriers['bottom'][0]
                if sq.friction:
                    if not self._impulse:
                        vector[0] /= 2
                        if abs(vector[0]) < 0.01:
                            vector[0] = 0
                    else:
                        self._impulse = False

        self.vector = Vector2(vector)
        self.cr += self.vector

        if self.clipping:
            tw, th = self.control.view.grid.get_tile_size()
            if self._need_adjust_r and self.gravity:
                if 'bottom' in barriers:
                    sq = barriers['bottom'][0]
                    _, r = self.control.view.get_cr(*self.midbottom)
                    # self.cr[1] = sq.row-0.5
                    if r > sq.row:
                        self.cr[1] -= (r - sq.row)
                    # tw, th = self.control.view.grid.get_tile_size()
                    # r = self.height / th / 2
                    # self.cr[1] = sq.row - r
                self._need_adjust_r = False

            if self._need_adjust_c:
                if 'left' in barriers:
                    sq = barriers['left'][0]
                    c, _ = self.control.view.get_cr(*self.midleft)
                    if c < sq.col:
                        self.cr[0] += (sq.col - c)
                if 'right' in barriers:
                    sq = barriers['right'][0]
                    c,_ = self.control.view.get_cr(*self.midright)
                    if c > sq.col:
                        self.cr[0] -= (c - sq.col)
                self._need_adjust_c = False

        for p in self.projectiles:
            p.update()

        self.projectiles = [
            p for p in self.projectiles if not p.deleted
        ]

    _cleanup_funcs = None
    @property
    def _cleanup_tasks(self):
        if self._cleanup_funcs is None:
            self._cleanup_funcs = set()
        return self._cleanup_funcs

    def cleanup_tasks(self):
        for func in self._cleanup_tasks:
            func()

    _fire_info = None
    def start_firing(self, projectile_class, rate=1.0):
        if self._fire_info is None and not self.deleted:
            self._fire_info = {
                'projectile_class': projectile_class,
                'rate': rate,
            }
            self._cleanup_tasks.add(self.stop_firing)
            # self._fire()
            pgzglobals.clock.schedule_interval(self._fire, rate)

    def _fire(self):
        fire = self._fire_info
        if fire:
            pclass = fire['projectile_class']
            self.fire_projectile(pclass)

    def stop_firing(self):
        if self._fire_info:
            pgzglobals.clock.unschedule(self._fire)
            self._fire_info = None
    
    _track_info = None
    def start_track(self, goal, refresh=0.5):
        if self._track_info is None and not self.deleted:
            self._track_info = {
                'goal': goal,
                'refresh': refresh,
            }
            self._cleanup_tasks.add(self.stop_track)
            pgzglobals.clock.schedule(self._track, 0.0)

    def stop_track(self):
        if self._track_info:
            pgzglobals.clock.unschedule(self._track)
            self._track_info = None
            self.vector = (0, 0)

    def _track(self):
        track = self._track_info
        path = self.control.get_path(self, track['goal'])
        if path:
            track['path'] = path[1:]
            if view.debug:
                self.control.highlight_squares(path)
            pgzglobals.clock.schedule(self._track, track['refresh'])

    def _update_track(self):
        track = self._track_info
        if track and 'path' in track:
            path = track['path']
            if path:
                c, r = self.cr
                next_sq, *rest = path
                ec, er = next_sq.col + 0.5, next_sq.row + 0.5
                move = ((ec - c), (er - r))
                self.vector = (0, 0)
                if math.hypot(*move) < 0.1:
                    track['path'] = rest
                dc, dr = move
                self.angle = Vector2(dc, -dr).as_polar()[-1]
                self.move(move)
            else:
                self.stop_track()

    def is_hit(self, projectile, source):
        if self.is_player:
            return source.is_hostile
        elif self.is_hostile:
            return source.is_player

    def hit(self, projectile, source):
        log.debug('%s hit by %s fired from %s', self, projectile, source)

    def destroy(self):
        self.deleted = True
        self.cleanup_tasks()

    def destroy_in(self, seconds):
        pgzglobals.clock.schedule(self.destroy, seconds)



class Projectile(Avatar):
    halo_dist = -0.3
    def __init__(self, source, control, cr, **kw):
        self.source = source
        super().__init__(control, cr, **kw)

    def update(self):
        super().update()

        mobs = self.check_mobs()
        is_hit = False
        for m in mobs:
            if m.is_hit(self, self.source):
                is_hit = True
                m.hit(self, self.source)
        if self.check_player():
            if self.control.player.is_hit(self, self.source):
                is_hit = True
                self.control.player.hit(self, self.source)
        if is_hit:
            self.deleted = True

        barriers = self.check_barriers()
        boundaries = self.check_boundaries()
        sides = set(barriers) | set(boundaries)
        if sides:
            self.deleted = True
