import pprint
import logging
from itertools import chain

from pygame.math import Vector2

from .square import GridSquare

log = logging.getLogger('grid')
log.addHandler(logging.NullHandler())

class Grid:
    def __init__(self, width, height, content, tiles):
        self.width = width
        self.height = height
        self.cols, self.rows = None, None  # will be init in level set

        self.raw_content = content
        self.tiles = tiles
        
        if content[0] == '\n':
            content = content[1:]
        if content[-1] == '\n':
            content = content[:-1]
        rows = content.splitlines()

        self.rows = len(rows)
        self.cols = max(len(row) for row in rows)

        log.debug(pprint.pformat(tiles))
        log.debug(pprint.pformat(rows))

        self.content = [[] for i in range(self.rows)]

        for i, row in enumerate(rows):
            for j, char in enumerate(row.ljust(self.cols)):
                square = GridSquare(j, i, char)
                if char in tiles:
                    square.extend(tiles[char])
                self.content[i].append(square)
        self.do_fill()

    def in_bounds(self, col, row=None):
        if row is None:
            col, row = col
        return (0 <= col < self.cols) and (0 <= row < self.rows)

    _tile_size = None
    def get_tile_size(self):
        if self._tile_size is None:
            sizes = [s.size for s in self.squares if s.size]
            if not sizes:
                return (self.width // self.cols, self.height // self.rows)
            self._tile_size = max(sizes, key=lambda s:s[0] * s[1])
        return self._tile_size
        
    @property
    def squares(self):
        return (s for row in self.content for s in row)

    @property
    def active_squares(self):
        return (s for s in self.squares if s.active)
        # return (s for s in self.squares if s.active)

    @property
    def highlighted(self):
        return (s for s in self.active_squares if s.highlight)

    @property
    def barriers(self):
        return (s for s in self.squares if s.barrier)
        return (s for s in self.active_squares if s.barrier)

    @property
    def transitions(self):
        return chain(*(s.transitions for s in self.squares))

    @property
    def spawns(self):
        return chain(*(s.spawns for s in self.squares))

    @property
    def mob_spawns(self):
        return chain(*(s.mob_spawns for s in self.squares))

    @property
    def item_spawns(self):
        return chain(*(s.item_spawns for s in self.squares))

    @property
    def player_spawns(self):
        return chain(*(s.player_spawns for s in self.squares))

    @property
    def player_start(self):
        for (s, sq) in self.spawns:
            if s.player and s.active:
                return (s, sq)

    def get_square(self, col, row):
        return self.content[int(row)][int(col)]

    def neighbors(self, col, row=None):
        if row is None:
            col, row = col
        nbors = [
            (col - 1, row - 1),
            (col - 1, row),
            (col - 1, row + 1),

            (col, row - 1),
            (col, row + 1),

            (col + 1, row - 1),
            (col + 1, row),
            (col + 1, row + 1)
        ]

        def not_barrier(p):
            return not self.get_square(*p).barrier

        return [p for p in nbors if self.in_bounds(p) and not_barrier(p)]

    def do_fill(self):
        fill_squares = [s for s in self.squares if s.fill]
        regions = [[(s, s.fill_cycles)] for s in fill_squares]
        while regions:
            region = regions.pop(0)
            (s, cycles), new_region = region[0], region[1:]
            if cycles > 0:
                for (c, r) in self.neighbors(s.col, s.row):
                    ns = self.get_square(c, r)
                    if not ns.tiles:  # i.e. empty square
                        ns[0:0] = s.tiles
                        new_region.append((ns, cycles - 1))
            if new_region:
                regions.append(new_region)

    def distance(self, p1, p2):
        s1 = self.get_square(*p1)
        s2 = self.get_square(*p2)
        cost = s1.cost + s2.cost
        if p1[0] == p2[0] or p1[1] == p2[1]:
            return cost
        else:
            return 2 * cost
