import shutil
import logging
from functools import lru_cache
from pathlib import Path
import os

from PIL import Image

log = logging.getLogger('grid')
log.addHandler(logging.NullHandler())

memoized = lru_cache(None)

class pgzglobals:
    @classmethod
    def getattrs(cls):
        return set(cls.__dict__) - {'getattrs'}

def clamp(v, min_v, max_v):
    if v < min_v:
        return min_v
    if v > max_v:
        return max_v
    return v
minmax = clamp

        
def ctrl_down(mod):
    return (mod & pgzglobals.keymods.LCTRL) or (mod & pgzglobals.keymods.RCTRL)

def sign(v):
    return 1 if v >= 0 else -1

def diffprint(v):
    if v != diffprint.last:
        print(v)
        diffprint.last = v
diffprint.last = None

def image_exists(image):
    for base, dirs, names in os.walk('images'):
        for ext in ['', '.png','.gif','.jpg']:
            path = Path(base, image + ext)
            if path.exists():
                return path
    return False

def is_number(v):
    try:
        float(v)
        return True
    except TypeError:
        return False

class TileLoadError(Exception):
    pass

def init_image(image, scale):
    path = image_exists(image)
    if not path:
        raise TileLoadError(f'path does not exist: {image}')

    orig_path = Path('images', path.name)
    if not orig_path.exists():
        shutil.copy(path, orig_path)

    if scale == 1:
        return image

    scale_name = f'scale_{scale}_{path.name}'.lower()
    scale_path = Path('images', scale_name)
    if scale_path.exists():
        return scale_path.stem

    print(scale_path)

    im = Image.open(path)
    w, h = im.size
    scale_im = im.resize(
        map(int,(w * scale, h * scale)), Image.LANCZOS,
    )
    scale_im.save(scale_path)
    return scale_path.stem


