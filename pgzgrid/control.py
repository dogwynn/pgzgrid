import logging
import inspect
from itertools import chain
from functools import lru_cache
from collections import ChainMap

import astar
import pygame
from pygame.math import Vector2

from .grid import Grid
from .view import GridView
from .square import GridSquare
from .avatar import Avatar
from .helpers import pgzglobals

log = logging.getLogger('grid')
log.addHandler(logging.NullHandler())

memoized = lru_cache(None)

class Level:
    initialized = False
    def __init__(self, content, control):
        self.content = content
        self.control = control
        self.mobs = []
        self.items = []

gamepad_lut = {
    'USB Gamepad ': {
        'axis': {
            'x': 3,
            'y': 4,
        },
        'button': {
            'a': 1, 'b': 2,
            'x': 0, 'y': 3,
            'l': 4, 'r': 5,
            'select': 8,
            'start': 9,
        },
    },

    'USB,2-axis 8-button gamepad  ': {
        'axis': {
            'x': 0,
            'y': 1,
        },
        'button': {
            'a': 0, 'b': 1,
            'x': 2, 'y': 3,
            'l': 4, 'r': 5,
            'select': 6,
            'start': 7,
        },
    },
}

def axis(val):
    if val < 0 and (1 - abs(val)) < 0.1:
        return -1
    if val > 0 and (1 - abs(val)) < 0.1:
        return 1
    return 0

class Gamepad:
    def __init__(self, joystick):
        joystick.init()
        self.name = joystick.get_name()
        self.joystick = joystick
        self.lut = gamepad_lut[self.name]

    @property
    def xaxis(self):
        return axis(self.joystick.get_axis(self.lut['axis']['x']))

    @property
    def yaxis(self):
        return axis(self.joystick.get_axis(self.lut['axis']['y']))

    @property
    def axis(self):
        x = self.xaxis
        y = self.yaxis
        dirs = set()
        if x > 0:
            dirs.add('right')
        elif x < 0:
            dirs.add('left')
        if y < 0:
            dirs.add('up')
        elif y > 0:
            dirs.add('down')
        return dirs

    @property
    def a(self):
        return self.joystick.get_button(self.lut['button']['a'])

    @property
    def b(self):
        return self.joystick.get_button(self.lut['button']['b'])
    
    @property
    def x(self):
        return self.joystick.get_button(self.lut['button']['x'])

    @property
    def y(self):
        return self.joystick.get_button(self.lut['button']['y'])

    @property
    def l(self):
        return self.joystick.get_button(self.lut['button']['l'])

    @property
    def r(self):
        return self.joystick.get_button(self.lut['button']['r'])

    @property
    def start(self):
        return self.joystick.get_button(self.lut['button']['start'])

    @property
    def select(self):
        return self.joystick.get_button(self.lut['button']['select'])

    @property
    def state(self):
        return {('axis', 'x', self.xaxis), ('axis', 'y', self.yaxis),
                ('button', 'a', self.a), ('button', 'b', self.b),
                ('button', 'x', self.x), ('button', 'y', self.y),
                ('button', 'l', self.l), ('button', 'r', self.r),
                ('button', 'start', self.start),
                ('button', 'select', self.select)}

    _last = None
    @property
    def diff(self):
        if self._last is None:
            self._last = self.state
        diff_ = self.state - self._last
        diff = {}
        for type, name, val in diff_:
            diff.setdefault(type, {})[name] = val
        self._last = self.state
        return diff

class GridControl:
    level_class = Level
    def __init__(self, width, height, content, tiles, *,
                 level_class=None, move_percent=None):
        self.width = width
        self.height = height
        self.content = content
        if level_class is not None:
            self.level_class = level_class
        self.levels = {
            name: self.level_class(text, self)
            for name, text in self.content.items()
        }
        self.move_percent = move_percent
        
        defaults = tiles.pop('defaults', {})

        def get_tiles(tile_set):
            new_set = {}
            for char, actors in tile_set.items():
                new_actors = actors[:]
                for i, actor in enumerate(actors):
                    if type(actor) is str:
                        T = ChainMap(tile_set, defaults)[actor]
                        new_actors[i:i+1] = T
                new_set[char] = new_actors
            return new_set

        self.tiles = {lk: get_tiles(tiles[lk]) for lk in tiles}
        for level_key in self.content:
            tile_set = self.tiles.get(level_key, {})
            new_set = defaults.copy()
            new_set.update(tile_set)
            self.tiles[level_key] = new_set

        self.gamepads = []
        self._pad_callbacks = {}

    initialized = False
    def init_engine(self):
        if not self.initialized:
            needed = {
                'screen', 'keys', 'keymods', 'mouse', 'keyboard',
                'clock',
            } - pgzglobals.getattrs()
            stack = inspect.stack()
            try:
                info = stack[1]
                nspace = info.frame.f_globals
                if needed.issubset(set(nspace)):
                    for name in needed:
                        setattr(pgzglobals, name, nspace[name])

                pygame.joystick.init()
                jcount = pygame.joystick.get_count()
                self.gamepads = [
                    Gamepad(pygame.joystick.Joystick(i)) for i in range(jcount)
                ]

                nspace['gamepads'] = self.gamepads
                for i in range(10):
                    if i < len(self.gamepads):
                        nspace[f'gp{i}'] = self.gamepads[i]
                    else:
                        nspace[f'gp{i}'] = None

                pad_callbacks = {
                    'on_pad_button_down', 'on_pad_button_up',
                    'on_pad_axis_down', 'on_pad_axis_up'
                }
                for name in set(nspace) & pad_callbacks:
                    self._pad_callbacks[name] = nspace[name]
                
            finally:
                del stack
            self.initialized = True
        
    def set_level(self, level_name, start=None, reset=False):
        self.level_name = level_name
        self.level = self.levels[level_name]
        tiles = self.tiles[self.level_name]
        self.grid = Grid(
            self.width, self.height, self.level.content, tiles
        )
        if type(start) is str:
            lut = {s.char: s for row in self.grid.content for s in row}
            start = lut.get(start)

        (spawn, square) = self.grid.player_start
        if start is None:
            start = square.col + 0.5, square.row + 0.5
        c, r = start
        self.view = GridView(self.grid, c, r)
        self.player = spawn.new_avatar(self, start)
        
        if reset:
            self.reset_level()
        else:
            self.init_level()

    def transition_level(self, transition, square, player):
        self.stop_level(self.level)
        self.level_name = transition.level_name
        name = self.level_name
        self.level = self.levels[name]
        tiles = self.tiles[name]
        self.grid = Grid(
            self.width, self.height, self.level.content, tiles
        )
        lut = {s.char: s for row in self.grid.content for s in row}
        start = lut.get(transition.char)
        c, r = start.col + 0.5, start.row + 0.5
        self.view = GridView(self.grid, c, r)
        self.player = player
        self.player.cr = (c, r)

        self.init_level()

    def spawn_avatars(self):
        for spawns, avatars in [(self.grid.mob_spawns, self.level.mobs),
                                (self.grid.item_spawns, self.level.items),]:
            for (spawn, square) in spawns:
                c, r = square.cr
                av = spawn.new_avatar(self, (c + 0.5, r + 0.5))
                avatars.append(av)
        
    def init_level(self):
        if not self.level.initialized:
            self.level.mobs = []
            self.level.items = []
            self.spawn_avatars()
            self.level.initialized = True

    def stop_level(self, level):
        for mob in level.mobs:
            mob.cleanup_tasks()

    def reset_level(self):
        self.level.initialized = False
        self.init_level()

    @property
    def items(self):
        return [i for i in self.level.items]
    
    @property
    def mobs(self):
        return [m for m in self.level.mobs]

    @property
    def hostiles(self):
        return [m for m in self.level.mobs if m.hostile]

    def square_from_xy(self, pos):
        col, row = self.view.get_cr(*pos)
        if self.grid.in_bounds(col, row):
            sq = self.grid.get_square(col, row)
            return sq

    def highlight_squares(self, *squares):
        if type(squares[0]) in {list, tuple}:
            squares = squares[0]

        if all(bool(s) for s in squares):
            for sq in self.grid.highlighted:
                sq.highlight = False
            for sq in squares:
                sq.highlight = True

    def player_path_to_xy(self, pos):
        goal = self.square_from_xy(pos)
        if goal:
            pc, pr = self.player.int_cr
            gc, gr = goal.int_cr
            p1, p2 = ((pc, pr), (gc, gr))
            return self.get_path(p1, p2)
        
    def get_path(self, start, end):
        if isinstance(start, (Avatar, GridSquare)):
            start = start.cr
        if isinstance(end, (Avatar, GridSquare)):
            end = end.cr

        start, end = tuple(map(int, start)), tuple(map(int, end))

        start = (start[0] + 0.5, start[1] + 0.5)
        end = (end[0] + 0.5, end[1] + 0.5)

        def cost(p, goal):
            return (goal[0] - p[0])**2 + (goal[1] - p[1])**2

        path = astar.find_path(
            start, end,
            neighbors_fnct=self.grid.neighbors,
            heuristic_cost_estimate_fnct=cost,
            distance_between_fnct=self.grid.distance,
        )

        if path:
            return [self.grid.get_square(c, r) for c,r in path]
        return []

    def draw_view(self):
        self.view.draw()
        
    def draw_player(self):
        self.player.draw()

    def draw_items(self):
        for item in self.level.items:
            item.draw()
        
    def draw_mobs(self):
        for mob in self.level.mobs:
            mob.draw()

    def draw(self):
        self.draw_view()
        self.draw_items()
        self.draw_player()
        self.draw_mobs()

    def move_view_by_player(self, side_percent):
        self.view.move_by_avatar(self.player, side_percent)

    def update_player(self):
        self.player.update()
        self.view.update_avatars([self.player] + self.player.projectiles)

    def update_mobs(self):
        for mob in self.level.mobs:
            mob.update()
        self.view.update_avatars(
            chain(self.level.mobs, *[m.projectiles for m in self.level.mobs])
        )

    def update_items(self):
        for item in self.level.items:
            item.update()
        self.view.update_avatars(
            self.level.items
        )

    def update_view(self):
        self.move_view_by_player(self.move_percent)
        self.view.update()

    def update_gamepads(self):
        cb_a_down = self._pad_callbacks.get('on_pad_axis_down')
        cb_a_up = self._pad_callbacks.get('on_pad_axis_up')
        cb_b_down = self._pad_callbacks.get('on_pad_button_down')
        cb_b_up = self._pad_callbacks.get('on_pad_button_up')
        for g in self.gamepads:
            diff = g.diff
            if 'axis' in diff:
                if 'x' in diff['axis']:
                    if diff['axis']['x']:
                        d = 'left' if diff['axis']['x'] < 0 else 'right'
                        if cb_a_down:
                            cb_a_down(g, 'x', d)
                    elif cb_a_up:
                        cb_a_up(g, 'x')
                else:
                    if diff['axis']['y']:
                        d = 'up' if diff['axis']['y'] < 0 else 'down'
                        if cb_a_down:
                            cb_a_down(g, 'y', d)
                    elif cb_a_up:
                        cb_a_up(g, 'y')
            if 'button' in diff:
                for button in ['a', 'b', 'x', 'y', 'l', 'r']:
                    if button in diff['button']:
                        if diff['button'][button] and cb_b_down:
                            cb_b_down(g, button)
                        elif cb_b_up:
                            cb_b_up(g, button)
                


    def update_all(self):
        self.update_player()
        self.update_level()

        self.update_mobs()
        self.update_level()

        self.update_items()
        self.update_level()

        self.update_view()
        self.update_level()

        self.update_gamepads()

    def update_level(self):
        self.level.mobs = [m for m in self.level.mobs if not m.deleted]
        self.level.items = [i for i in self.level.items if not i.deleted]

    def _call_event_callbacks(self, callback, *args):
        if hasattr(self.view, callback):
            getattr(self.view, callback)(*args)
        if hasattr(self.player, callback):
            getattr(self.player, callback)(*args)
        for m in self.level.mobs:
            if hasattr(m, callback):
                getattr(m, callback)(*args)

    def on_key_up(self, key, mod):
        self._call_event_callbacks('on_key_up', key, mod)

    def on_key_down(self, key, mod):
        self._call_event_callbacks('on_key_down', key, mod)
        
    def on_mouse_up(self, pos, button):
        self._call_event_callbacks('on_mouse_up', pos, button)

    def on_mouse_down(self, pos, button):
        self._call_event_callbacks('on_mouse_down', pos, button)

    def on_mouse_move(self, pos, rel, buttons):
        self._call_event_callbacks('on_mouse_move', pos, rel, buttons)

    def on_pad_axis_down(self, pad, axis, direction):
        self._call_event_callbacks('on_pad_axis_down', pad, axis, direction)

    def on_pad_axis_up(self, pad, axis):
        self._call_event_callbacks('on_pad_axis_up', pad, axis)

    def on_pad_button_down(self, pad, button):
        self._call_event_callbacks('on_pad_button_down', pad, button)

    def on_pad_button_up(self, pad, button):
        self._call_event_callbacks('on_pad_button_up', pad, button)
