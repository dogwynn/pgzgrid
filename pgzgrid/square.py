import logging

from pygame.math import Vector2

from . import tile as _tile
from .tile import (
    Tile, Spawn, Transition,
)

log = logging.getLogger('grid')
log.addHandler(logging.NullHandler())

class GridError(Exception):
    pass

class GridSquare(list):
    def __init__(self, col, row, char, *, color='grey'):
        self.col = col
        self.row = row
        self.char = char
        self.color = color

    @property
    def cr(self):
        return Vector2(self.col, self.row)

    @property
    def int_cr(self):
        return Vector2(*map(int, self.cr))

    def __hash__(self):
        return hash((self.col, self.row))

    def __str__(self):
        items = ", ".join(v.__class__.__name__[:10] for v in self)
        return f'<({self.col},{self.row}): [{items}]>)'

    def __repr__(self):
        return str(self)

    def has_tile(self, *tile_classes):
        return [tile for tile in self if isinstance(tile, tile_classes)]
                

    _highlight = False
    @property
    def highlight(self):
        return self._highlight
    @highlight.setter
    def highlight(self, value):
        self._highlight = bool(value)

    _active = False
    @property
    def active(self):
        return self._active
    @active.setter
    def active(self, value):
        self._active = bool(value)

    @property
    def tiles(self):
        return [item for item in self if isinstance(item, Tile)]

    @property
    def barrier(self):
        return any(tile.barrier for tile in self.tiles)

    @property
    def cost(self):
        return sum(tile.cost for tile in self.tiles)

    @property
    def max_cost(self):
        return max(tile.cost for tile in self.tiles)

    @property
    def size(self):
        tiles = self.tiles
        if tiles:
            return (tiles[0].width, tiles[0].height)

    @property
    def fill(self):
        return any(tile.fill for tile in self.tiles)

    @property
    def fill_cycles(self):
        return max(tile.fill_cycles for tile in self.tiles)

    @property
    def transitions(self):
        return [(item, self) for item in self if isinstance(item, Transition)]

    @property
    def spawns(self):
        return [(item, self) for item in self if isinstance(item, Spawn)]

    @property
    def mob_spawns(self):
        return ((s, sq) for s,sq in self.spawns
                if (not s.player) and (not s.item) and s.active)

    @property
    def item_spawns(self):
        return ((s, sq) for s,sq in self.spawns if s.item and s.active)

    @property
    def hostile_spawns(self):
        return ((s, sq) for s,sq in self.spawns if s.hostile)

    @property
    def player_spawns(self):
        return [(s, sq) for s, sq in self.spawns if s.player and s.active]

    @property
    def friction(self):
        return any(tile.friction for tile in self.tiles)
