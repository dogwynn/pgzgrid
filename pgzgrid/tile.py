import math
import logging

from pgzero.actor import Actor

from .helpers import (
    init_image,
)

log = logging.getLogger('grid')
log.addHandler(logging.NullHandler())

def parse_place(place, default):
    place = default if place is None else place
    for i,v in enumerate(place):
        if not 0 <= v < 1:
            place[i] = v / 100
    return place

class Tile(Actor):
    scale = 1.0
    barrier = False
    barrier_sides = None
    default_place = (0.5, 0.5)
    place = None
    fill = False
    friction = False
    cost = 0

    def __init__(self, image, place=None, scale=None, **kw):
        self.place = parse_place(place, self.__class__.default_place)
        for key in list(kw):
            if hasattr(self, key):
                setattr(self, key, kw.pop(key))

        self.scale = scale if scale is not None else self.__class__.scale
        image = init_image(image, self.scale)
        
        super().__init__(image, (-1, -1), **kw)

    def init_from_grid(self, grid):
        perc_w, perc_h = self.place
        W, H = grid.get_tile_size()
        self.pos = perc_w * W, perc_h * H


        
class Floor(Tile):
    pass

class Ground(Tile):
    pass

class Barrier(Tile):
    barrier = True
    
class Wall(Barrier):
    pass

class Platform(Tile):
    barrier = True
    friction = True

class Fill:
    fill = True
    fill_cycles = math.inf

class FloorFill(Fill, Floor):
    pass

class GroundFill(Fill, Ground):
    pass

class BarrierFill(Fill, Barrier):
    pass

class SpawnError(Exception):
    pass

class Spawn:
    player = False
    hostile = False
    active = True
    item = False
    avatar_class = None
    def __init__(self, avatar_class=None):
        if avatar_class is not None:
            self.avatar_class = avatar_class
        if self.avatar_class is None:
            raise SpawnError(
                'Spawn should either have Avatar class given at'
                ' class level (i.e. in subclass) or as a'
                ' constructor argument'
            )

    def new_avatar(self, control, cr):
        av = self.avatar_class(
            control, cr, image=self.avatar_class.start_image,
        )
        av.is_player = self.player
        av.is_hostile = self.hostile
        av.is_item = self.item
        return av


class MobSpawn(Spawn):
    player = False
    hostile = False

class ItemSpawn(Spawn):
    item = True

class HostileSpawn(MobSpawn):
    hostile = True
    
class PlayerStart(Spawn):
    player = True

class Checkpoint(Spawn):
    player = True
    active = False

class Transition:
    def __init__(self, level_name, char):
        self.level_name = level_name
        self.char = char
    
