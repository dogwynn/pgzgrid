import logging

import pygame
import pgzero

from .helpers import (
    clamp, ctrl_down, pgzglobals,
)

log = logging.getLogger('grid')
log.addHandler(logging.NullHandler())

debug = False

def configure_debug():
    logging.basicConfig()
    if debug:
        level = logging.DEBUG
    else:
        level = logging.WARN
    logging.getLogger().setLevel(level)
    log.setLevel(level)
    for handler in log.handlers:
        handler.setLevel(level)

    log.debug('DEBUGGING ON!')
        

class GridView:
    def __init__(self, grid, col, row):
        self.grid = grid
        self.col = col
        self.row = row
        self.reset()

    def reset(self):
        self.image = None
        self.barriers = []

        self.pgclock = pygame.time.Clock()
        self.fps = 0
        
    def __getitem__(self, index):
        if type(index) in {tuple, list}:
            col, row = index
            return self.grid.get_square(col, row)
        raise IndexError('must provide (col, row) tuple to view[]')

    def grid_coordinates(self):
        width, height = self.grid.width, self.grid.height
        tile_w, tile_h = self.grid.get_tile_size()

        half_w, half_h = width / 2, height / 2

        start_col_f = self.col - (half_w / tile_w)
        start_col = clamp(
            int(start_col_f), 0, self.grid.cols - 1
        )

        sx = half_w - (
            # half_tw +
            (self.col - start_col) * tile_w
        )

        end_col_f = self.col + (half_w / tile_w)
        end_col = min(int(end_col_f + 1) + 1, self.grid.cols)

        start_row_f = self.row - (half_h / tile_h)
        start_row = clamp(
            int(start_row_f), 0, self.grid.rows - 1
        )

        sy = half_h - (
            # half_th +
            (self.row - start_row) * tile_h
        )

        end_row_f = self.row + (half_h / tile_h)
        end_row = min(int(end_row_f + 1) + 1, self.grid.rows)

        ex, ey = (sx + (end_col - start_col) * tile_w,
                  sy + (end_row - start_row) * tile_h)

        return start_col, end_col, start_row, end_row, sx, ex, sy, ey

    def get_xy(self, col, row):
        (start_col, end_col,
         start_row, end_row,
         sx, ex, sy, ey) = self.grid_coordinates()

        tile_w, tile_h = self.grid.get_tile_size()

        x = sx + (col - start_col)*tile_w
        y = sy + (row - start_row)*tile_h

        return x, y
    
    def get_cr(self, x, y):
        (start_col, end_col,
         start_row, end_row,
         sx, ex, sy, ey) = self.grid_coordinates()

        tile_w, tile_h = self.grid.get_tile_size()

        col = (x - sx) / tile_w + start_col
        row = (y - sy) / tile_h + start_row
        
        return col, row

    def draw(self):
        (start_col, end_col,
         start_row, end_row,
         sx, ex, sy, ey) = self.grid_coordinates()

        tile_w, tile_h = self.grid.get_tile_size()

        for sq in self.grid.squares:
            sq.active = False

        self.barriers = []
        for sq in self.grid.barriers:
            x, y = self.get_xy(sq.col, sq.row)
            rect = pygame.Rect(x, y, tile_w, tile_h)
            self.barriers.append((sq, rect))
            
        x, y = sx, sy
        for row in range(start_row, end_row):
            for col in range(start_col, end_col):
                square = self.grid.get_square(col, row)
                square.active = True
                rect = pygame.Rect(x, y, tile_w, tile_h)
                # if square.barrier:
                #     self.barriers.append((square, rect))
                self.draw_grid_square(square, rect, pgzglobals.screen)
                x += tile_w
            y += tile_h
            x = sx

        if debug:
            self.draw_grid_lines()
            self.draw_fps()

    def draw_grid_square(self, square, rect, screen):
        if square:
            for tile in square.tiles:
                tile.pos = rect.center
                tile.draw()
            if square.highlight:
                surf = pygame.Surface((rect.width, rect.height))
                surf.set_alpha(50)
                surf.fill((255,255,255))
                screen.surface.blit(surf, (rect.x, rect.y))

    def draw_fps(self):
        pgzglobals.screen.draw.text(
            str(round(self.fps)),
            color='white',
            midtop=(self.grid.width - 10, 10),
            fontsize=24,
            shadow=(1, 1)
        )
                
    def draw_grid_lines(self):
        (start_col, end_col,
         start_row, end_row,
         sx, ex, sy, ey) = self.grid_coordinates()

        tile_w, tile_h = self.grid.get_tile_size()

        x, y = sx, sy

        for row_i in range(start_row, end_row + 1):
            pygame.draw.line(
                pgzero.game.screen, (255, 255, 255),
                (sx, y), (ex, y),
            )
            y += tile_h

        for col_i in range(start_col, end_col + 1):
            pygame.draw.line(
                pgzero.game.screen, (255, 255, 255),
                (x, sy), (x, ey),
            )
            x += tile_w

    def move_by_avatar(self, avatar, percent, speed=0.5):
        width, height = self.grid.width, self.grid.height
        x, y = self.get_xy(*avatar.cr)
        col, row = self.col, self.row
        dc, dr = map(abs, avatar.vector)

        if x < width * percent:
            col -= dc

        if width * (1 - percent) <= x:
            col += dc

        if y < height * percent:
            row -= dr

        if height * (1 - percent) <= y:
            row += dr

        self.col, self.row = col, row

    def update_avatars(self, avatars):
        for a in avatars:
            x, y = self.get_xy(*a.cr)
            a.pos = (x,y)

    def update(self):
        self.pgclock.tick(60)
        self.fps = self.pgclock.get_fps()

    def on_key_down(self, key, mod):
        global debug
        log.debug('%s %s %s', key, int(key), mod)
        if key == key.BACKQUOTE and ctrl_down(mod):
            debug = not debug
            configure_debug()

    def on_key_up(self, key, mod):
        pass

