from pgzgrid import (
    GridControl,

    Tile, Wall, Floor, Fill, Ground, GroundFill, FloorFill,
    PlayerStart, Spawn, MobSpawn, HostileSpawn, ItemSpawn,
    Transition,

    Platform,

    Avatar, Projectile,
)
from pygame.math import Vector2

WIDTH = 640
HEIGHT = 480

Avatar.scale = 0.5
Tile.scale = 0.5

class Player(Avatar):
    start_image = 'robot1_machine'
    max_vector = (0.1, 0.1)

    def on_key_down(self, key, mod):
        if key == keys.SPACE:
            self.fire_projectile(Toaster)

    def on_key_up(self, key, mod):
        self.vector = (0, 0)

    def update(self):
        if keyboard.w or (gamepads and 'up' in gp0.axis):
            self.move((0, -1))
        if keyboard.a or (gamepads and 'left' in gp0.axis):
            self.move((-1, 0))
        if keyboard.d or (gamepads and 'right' in gp0.axis):
            self.move((1, 0))
        if keyboard.s or (gamepads and 'down' in gp0.axis):
            self.move((0, 1))

        self.angle = self.view_angle

        items = self.check_items()
        if items:
            for item in items:
                item.destroy()

        super().update()


class Zombie(Avatar):
    start_image = 'zoimbie1_hold'
    max_vector = (0.05, 0.05)
    stunned = False
    hp = 10
    def update(self):
        if self.alive and not self.stunned:
            self.start_track(self.control.player)
        super().update()

    def awake(self):
        self.stunned = False

    def hit(self, projectile, source):
        self.stop_track()
        if self.alive:
            self.move(projectile.view_vector)
            self.stunned = True
            clock.schedule_unique(self.awake, 0.1)

        self.hp -= 1
        if self.hp == 0:
            self.alive = False
            self.vector = (0, 0)
            self.destroy_in(2)


class ZombieSpawn(HostileSpawn):
    avatar_class = Zombie


class Toaster(Projectile):
    start_image = 'tile_269'
    max_vector = (0.5, 0.5)

class MagicToaster(Avatar):
    start_image = 'tile_269'
    def draw(self):
        super().draw()


tiles = {
    'defaults': {
        'x': [Wall('tile_12')],
        '_': [FloorFill('tile_42')],
        '-': [Floor('tile_42')],
        'g': [GroundFill('tile_01')],
        'z': [ZombieSpawn()],
        't': [ItemSpawn(MagicToaster)],
    },

    'world_map': {
        'S': [PlayerStart(Player)],
        '2': ['-', Tile('tile_269'), Transition('second_floor', '1')]
    },

    'second_floor': {
        '1': ['-', Transition('world_map','2')]
    }
}

content = {
    'world_map': '''



   xxxxxxxxxxxxxxxxxx
   x    2           x
   x                x
g  -         _      x
   x  t             x                  z    g
   x                x               z
   x                x                 z
   x  S     z       x
   x                x
   xxxxxxxxxxxxxxxxxx

    

''',
    'second_floor': '''
xxxxxxxx
x      x
x      x
x  1   x
xxxxxxxx
'''
}

control = GridControl(
    WIDTH, HEIGHT, content, tiles,
    move_percent=0.25,
)
control.set_level('world_map')

def on_key_down(key, mod):
    control.on_key_down(key, mod)


def on_key_up(key, mod):
    control.on_key_up(key, mod)

def update():
    control.init_engine()

    control.update_all()

def draw():
    screen.fill('black')
    control.draw()

