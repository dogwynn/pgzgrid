from pgzgrid import (
    GridControl,

    Tile, Wall, Floor, Fill, Ground, GroundFill, FloorFill, BarrierFill,
    PlayerStart, Spawn, MobSpawn, HostileSpawn, ItemSpawn,
    Transition,

    Platform,

    Avatar, Projectile,
)

from pygame.math import Vector2

WIDTH = 640
HEIGHT = 480

Avatar.scale = 0.5
Tile.scale = 0.5

class Road(Tile):
    pass

class Sand(Ground):
    pass

class SandFill(Fill, Sand):
    cost = 2

class Player(Avatar):
    start_image = 'car_red_1'
    rotate = 90
    scale = 0.25
    
    max_vector = (0.05, 0.05)

    def update(self):
        square = self.control.grid.get_square(*self.cr)
        if square.cost:
            self.max_vector = Vector2(Player.max_vector) / square.cost
        else:
            self.max_vector = Player.max_vector

        if keyboard.left or (gamepads and 'left' in gp0.axis):
            self.angle += 3
        if keyboard.right or (gamepads and 'right' in gp0.axis):
            self.angle += -3
        if keyboard.SPACE or (gamepads and gp0.b):
            self.move(self.angle_vector)

        super().update()

    def on_key_up(self, key, mod):
        self.vector = (0, 0)

            

    

tiles = {
    'defaults': {
        '│': [Road('road_asphalt01')],
        '─': [Road('road_asphalt02')],
        '┌': [Sand('land_sand05'), Road('road_asphalt03')],
        '┐': [Sand('land_sand05'), Road('road_asphalt05')],
        '└': [Sand('land_sand05'), Road('road_asphalt39')],
        '┘': [Sand('land_sand05'), Road('road_asphalt41')],
        '├': [Road('road_asphalt55')],
        '┬': [Road('road_asphalt56')],
        '┼': [Road('road_asphalt57')],
        '┴': [Road('road_asphalt73')],
        '┤': [Road('road_asphalt74')],
        'b': [SandFill('land_sand05')],
    },

    'stage1': {
        'S': [Road('road_asphalt43'), PlayerStart(Player)],
    },
}

content = {
    'stage1': '''
   
   ┌───S───┐
   │       │
   │  ┌─┐  │
   │  │ │  └─────┐  
   └──┘ │  b     │
    b   │        │
   ┌────┘  ┌─────┘
   │       │
   └───────┘
   b
   ┌─┬─┬─┐
   │ │ │ │ │
   └─┘ └─┴─┼───┤
           │
   
   
''',
}

control = GridControl(
    WIDTH, HEIGHT, content, tiles,
    move_percent=0.45,
)
control.set_level('stage1')

def on_key_down(key, mod):
    control.on_key_down(key, mod)

def on_key_up(key, mod):
    control.on_key_up(key, mod)

def update():
    control.init_engine()

    control.update_all()

def draw():
    screen.fill('black')
    control.draw()
